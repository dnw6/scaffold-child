<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div class="site-content">
 *
 * @link       https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package    scaffold
 * @copyright  Copyright (c) 2017, Danny Cooper
 * @license    http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Assistant:wght@300;400&family=Raleway:wght@300;400&display=swap" rel="stylesheet">

	<meta name="p:domain_verify" content="2775612d65c50cb763d82760f1e64077"/>
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135038573-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-135038573-1');
</script>
<meta name="p:domain_verify" content="13f372f23564fdace0e19aaccb6bed62"/>
</head>

<body <?php body_class(); ?>>

<div class="site-wrapper dani">

<div class="social-icons">
	<ul class="">
		<li><a href="https://www.etsy.com/uk/shop/DaniWilliamsArt" target="_blank"><?php get_template_part( 'template-parts/etsy-icon' ); ?></a></li>
		<li><a href="https://www.instagram.com/daniwilliamsillustration/" target="_blank"><?php get_template_part( 'template-parts/instagram-icon' ); ?></a></li>
		<li><a href="mailto:hello@daniwilliams.co.uk" title="hello@daniwilliams.co.uk"><?php get_template_part( 'template-parts/email-icon' ); ?></a></li>
		<li><a href="https://daniwilliams.substack.com/" target="_blank"><?php get_template_part( 'template-parts/substack-icon' ); ?></a></li>
		<li><a href="https://www.youtube.com/channel/UCxvdgjWIoBFWjjo_gX9pJEQ" target="_blank"><?php get_template_part( 'template-parts/youtube-icon' ); ?></a></li>
	</ul>
</div>

	<header class="site-header">
		<div class="wrapper">
			<?php get_template_part( 'template-parts/branding' ); ?>
		</div><!-- .wrapper -->
	</header><!-- .site-header -->

	<?php get_template_part( 'template-parts/menu-1' ); ?>


	<div class="site-content">
		<div class="wrapper">
