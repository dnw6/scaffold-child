<?php
function my_theme_enqueue_styles() {

    $parent_style = 'scaffold-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );


function my_theme_scripts() {
   // wp_enqueue_script( 'jquery-3.3.1.slim.min', get_template_directory_uri() . '/assets/js/jquery-3.3.1.slim.min.js', array( 'jquery' ), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'my_theme_scripts' );

add_image_size( 'tall-thumbnail', 150, 9999, false ); // width, height, crop

add_filter( 'image_size_names_choose', 'wpshout_custom_sizes' );
function wpshout_custom_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'tall-thumbnail' => __( 'Tall Thumbnail' ),
    ) );
}

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

/**
 * Enable ACF 5 early access
 * Requires at least version ACF 4.4.12 to work
 */
define('ACF_EARLY_ACCESS', '5');

add_action( 'wp_enqueue_scripts', function() {

	if ( ! defined( 'WPEX_THEME_STYLE_HANDLE' ) ) {
		return;
	}

	// First de-register the main child theme stylesheet
	wp_deregister_style( WPEX_THEME_STYLE_HANDLE );

	// Then add it again, using filemtime for the version number so everytime the child theme changes so will the version number
	wp_register_style( WPEX_THEME_STYLE_HANDLE, get_stylesheet_uri(), array(), filemtime( get_stylesheet_directory() . '/style.css' ) );

	// Finally enqueue it again
	wp_enqueue_style( WPEX_THEME_STYLE_HANDLE );
} );

?>

