<?php
/**
 * Template Name: Navigation Boxes Page Template
 * Template Post Type: post, page
 *
 */

get_header(); ?>

	<?php
		while ( have_posts() ) :
			the_post(); ?>

	<div class="content-area">

    <h2 class="text--center"><?php the_field('top_page_title'); ?></h2>

<div class="navbox-wrapper">




<?php if( have_rows('navbox') ): ?>

	<?php while( have_rows('navbox') ): the_row(); 

		// vars
		$image = get_sub_field('navbox_image');
		$title = get_sub_field('navbox_title');
		$subtitle = get_sub_field('navbox_subtitle');
        $link = get_sub_field('navbox_link');

        ?>
        
            <div class="navbox shop-block" style="background-image: url('<?php echo $image; ?>')">
                <a href="<?php echo $link; ?>" <?php if ( get_sub_field( 'navbox_link_target' ) ): ?> target="_blank"<?php endif; ?>>

			<?php if( $title ): ?>
				<div class="copy">
                    <h3><?php echo $title; ?></h3>
                    <p class="body-text"><?php echo $subtitle; ?></p>
                </div>
			<?php endif; ?>
                </a>
            </div>

	<?php endwhile; ?>

<?php endif; ?>

</div>
	

			<?php get_template_part( 'template-parts/content', 'page' );

		endwhile;
		?>

	</div><!-- .content-area -->

<?php
get_footer();
