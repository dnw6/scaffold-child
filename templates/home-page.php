<?php
/**
 * Template Name: Homepage Template
 * Template Post Type: post, page
 *
 * @package    scaffold
 * @copyright  Copyright (c) 2017, Danny Cooper
 * @license    http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */

get_header(); ?>

	<div class="body--centered">
	<div>
		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );


		endwhile;
		?>
		</div>
		

	</div><!-- .content-area -->

<?php
get_footer();