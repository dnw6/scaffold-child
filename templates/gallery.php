<?php
/**
 * Template Name: Image page
 * Template Post Type: post, page
 *
 * @package    scaffold
 * @copyright  Copyright (c) 2017, Danny Cooper
 * @license    http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */

get_header(); ?>

	<div class="">
		<div class="text--center back-link">
			<?php if ( $post->post_parent ) { ?>
			&#9668; <a href="<?php echo get_permalink( $post->post_parent ); ?>" class="">
				<?php echo get_the_title( $post->post_parent ); ?>
			</a>
			<?php } ?>
		</div>

		<h1 class="page-title text--center"><?php single_post_title(); ?></h1>

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile;
		?>

	</div><!-- .content-area -->

<?php
get_footer();
