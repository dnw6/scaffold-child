<svg width="512" height="512" class="soc-icon" viewBox="0 0 512 512" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect width="512" height="512" rx="60" fill="#A28B77"/>
<g clip-path="url(#clip0_1_13)">
<path fill-rule="evenodd" clip-rule="evenodd" d="M97 74H418V119H97V74ZM97 238.142H418V440.478L257.467 350.438L97 440.478V238.142ZM97 156.071H418V201.071H97V156.071Z" fill="white"/>
</g>
<defs>
<clipPath id="clip0_1_13">
<rect width="321" height="366.478" fill="white" transform="translate(97 74.0248)"/>
</clipPath>
</defs>
</svg>



